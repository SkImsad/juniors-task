export function getUsers(connectClient) {
  return new Promise((resolve, reject) => {
    connectClient.emit('users', (error, users) => {
      if (error != null) {
        reject(error)
        return
      }
      resolve(users)
    })
  })
  .then(response => response.reduce(
    (usersed, user) => Object.assign({}, usersed, {
      [user.id]: user.name
    }),
  {}
  ))
}
