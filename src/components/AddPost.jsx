import React from 'react'
import { addPost } from '../reducer/posts.js'
import { connect } from 'react-redux'
import { connectClient } from '../index.js'

export class AddPost extends React.Component {
  render() {
    return (
      <form
        className="form-inline"
      >
        <div
          className="form-group"
        >
          <input
            className="form-control"
            placeholder="Имя"
            type="text"
            ref={element => {
              this.name = element
            }}
          />
        </div>
        <div
          className="form-group"
        >
          <input
            className="form-control"
            placeholder="Текст"
            type="text"
            ref={element => {
              this.text = element
            }}
          />
        </div>
        <div
          className="form-group"
        >
          <button
            type="button"
            className="btn btn-success dropdown-toggle"
            onClick={
              () => {
                if (!this.name.value.trim() || !this.text.value.trim()) {
                  this.name.value = ''
                  this.text.value = ''
                  return
                }
                connectClient.emit('addPost', {
                  name: this.name.value,
                  text: this.text.value
                }, (error, addPostResponse) => {
                  if (error != null) {
                    document.getElementById('errorblock').style.display = 'none'
                    setTimeout(() => {
                      document.getElementById('errorblock').style.display = ''
                      document.getElementById('nameErrorblock').innerHTML = error.message
                    }, 100)
                    return
                  }
                  document.getElementById('errorblock').style.display = 'none'
                  this.props.addPost({
                    userId: addPostResponse.userId,
                    id: addPostResponse.postId,
                    name: this.name.value,
                    text: this.text.value
                  })
                  console.log('ok')
                  this.name.value = ''
                  this.text.value = ''
                })
              }
            }
          >
          Добавить
          </button>
        </div>
        <div id="errorblock"
          style={{ display: 'none', color: 'red' }}>
          <h3 id="nameErrorblock"></h3>
        </div>
      </form>
    )
  }
}

function dispatchToProps(dispatch) {
  return {
    addPost(post) { // eslint-disable-line no-shadow
      dispatch(
        addPost(post)
      )
    }
  }
}

export default connect(null, dispatchToProps)(AddPost)
